//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Layouts.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiSum;
	TTabItem *tiSrok;
	TEdit *EditSum;
	TLabel *Label1;
	TLabel *Label6;
	TLabel *Label7;
	TTabItem *tiPr;
	TLabel *Label4;
	TLabel *Label3;
	TEdit *EditSrok;
	TLayout *Layout2;
	TLayout *Layout3;
	TLayout *Layout4;
	TLabel *Label8;
	TLabel *Label9;
	TEdit *EditPr;
	TTabItem *tiResult;
	TLabel *laDohod2;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buBack;
	TButton *buNext;
	TButton *Button1;
	TLabel *dohod;
	TLabel *laDohod;
	TRadioButton *RadioButton1;
	TStyleBook *StyleBook1;
	void __fastcall buNextClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
