﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string[] textArray = textBox1.Text.Split(new char[] { ' ', '.', ',', '\r'}, StringSplitOptions.RemoveEmptyEntries);
            label2.Text = textArray.Length.ToString();
        }
    }
}
